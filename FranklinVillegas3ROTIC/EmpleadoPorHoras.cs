﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranklinVillegas3ROTIC
{
    class EmpleadoPorHoras
    {
        private String empleadoHoras;

        public String EmpleadoHoras 
        {
            get { return empleadoHoras; }
            set { empleadoHoras= value; }
        }
        private double NHoras= 0;
        private double PHoras = 2;

        private double sueldo;

        public void LeerNumero()
        {
            Console.WriteLine( "Ingrese número de horas "); NHoras = Convert.ToInt32(Console.ReadLine());
        }
        public double Sueldo 
        {
            get { return sueldo; }
            set { sueldo = value; }
        }

        public void Procedimiento()
        {
            sueldo = PHoras * NHoras;
        }

        public void Imprimir()
        {
            Console.WriteLine( sueldo );
        }
        
    }
}
