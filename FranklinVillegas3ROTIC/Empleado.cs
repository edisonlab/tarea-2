﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranklinVillegas3ROTIC
{
    class Empleado
    {
        //Atributos
        private String nombre;

        public  String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int edad;

        public  int Edad 
        {
            get { return edad ; }
            set { edad= value; }
        }

        private String departamento;

        public String Deparatamento
        {
            get { return departamento ; }
            set { departamento  = value; }
        }

        //Ingresar datos

        public void InngresarDatos()
        {
            Console.WriteLine("Ingrese sus nombres ");
            nombre = Console.ReadLine();
            Console.WriteLine("Ingrese su edad ");
            edad= Convert.ToInt32 (Console.ReadLine());
            Console.WriteLine("Ingrese su departamento  ");
            departamento = Console.ReadLine();
        } 
        // Mostrar datos 
        public void MostrarDatos()
        {
            Console.WriteLine(" Señor " + nombre + " edad "+ edad+ " del departemento de "+departamento );
        }


    }
}
